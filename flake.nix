{
  description = "Blog at https://place-holder.ru";

  inputs.systems.url = "github:nix-systems/default";

  outputs =
    {
      self,
      systems,
      nixpkgs,
    }:
    let
      eachSystem =
        fun:
        nixpkgs.lib.genAttrs (import systems) (
          system:
          let
            pkgs = nixpkgs.legacyPackages.${system};
          in
          fun system pkgs
        );
    in
    {
      packages = eachSystem (
        system: pkgs:
        let
          blog = pkgs.stdenv.mkDerivation {
            pname = "place-holder.ru";
            version = "0.1.0";

            src = pkgs.nix-gitignore.gitignoreSource [ ] ./.;

            nativeBuildInputs = [ pkgs.hugo ];

            buildPhase = "hugo";

            installPhase = ''
              mkdir $out
              cp -r public $out
            '';
          };
        in
        {
          inherit blog;
          default = blog;
        }
      );

      devShells = eachSystem (
        system: pkgs: {
          default = pkgs.mkShell {
            name = "blog";
            buildInputs = [ pkgs.hugo ];
            TERM = "xterm-256color";
          };
        }
      );

      checks = eachSystem (
        system: pkgs:
        let
          blog = self.packages.${system}.default;
        in
        {
          index =
            pkgs.runCommand "check-blog-index"
              { EXPECTED_BLOG_TITLE = "<title>Untitled — Emacs, Programming, and Anything</title>"; }
              ''
                if grep --fixed-strings "$EXPECTED_BLOG_TITLE" ${blog}/public/index.html; then
                  touch $out
                else
                  echo "Expected index.html to contain $EXPECTED_BLOG_TITLE" >&2
                  exit 1
                fi
              '';
        }
      );
    };
}
