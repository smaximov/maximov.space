# Terminal

![Terminal](https://github.com/panr/hugo-theme-terminal/blob/master/images/screenshot.png?raw=true)

This is the [Terminal](https://github.com/panr/hugo-theme-terminal/) Hugo theme
originally developed by [@panr](https://twitter.com/panr) with changes made to suit my
needs. All credits goes to him and other contributors.

## Licence

Copyright © 2019 Radosław Kozieł ([@panr](https://twitter.com/panr))

The theme is released under the MIT License. Check the [original theme license](https://github.com/panr/hugo-theme-terminal/blob/master/LICENSE.md) for additional licensing information.
